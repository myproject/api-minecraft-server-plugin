package fr.nuroz.apimcserver.api_for_spigot_server;

import com.sun.net.httpserver.HttpServer;
import org.bukkit.plugin.java.JavaPlugin;
import fr.nuroz.apimcserver.api_for_spigot_server.routes.Players;

import java.io.IOException;
import java.net.InetSocketAddress;

public final class API_for_spigot_server extends JavaPlugin {

    final int PORT = 8080;
    HttpServer server;

    @Override
    public void onEnable() {
        // Plugin startup logic
        try {
            server = HttpServer.create(new InetSocketAddress(PORT), 0);
            System.out.println("INFO API: Web server create succesfully");
        } catch (IOException e) {
            System.out.println("ERROR API: Cannot create web server");
            System.out.println("ERROR API: Cause: " + e.getMessage());
        }

        if(server != null) {
            server.createContext("/", new Players());
            System.out.println("INFO API: setup routes ok");

            server.setExecutor(null);
            server.start();
            System.out.println("INFO API: server start ok");
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
